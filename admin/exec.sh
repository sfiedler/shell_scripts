#!/bin/bash

# Automatic Executable Update Script (for git)
# 2024 by Samuel Fiedler

progname=""
build_system=""
prefix="none"
configargs=""
buildargs=""
install_prefix=""
file="exec.ini"

if [ "$1" = "--big" ]; then
    file="exec_big.ini"
fi

# iterate over just read file
cat $file | while read line; do
    if [ "$line" = "[program]" ]; then
        # program directive
        progname=""
        build_system=""
        prefix="none"
        configargs=""
        buildargs=""
        install_prefix=""
    elif [ "program_name=${line#'program_name='}" = "$line" ]; then
        # program name
        progname=${line#'program_name='}
    elif [ "build_system=${line#'build_system='}" = "$line" ]; then
        # build system name
        build_system=${line#'build_system='}
    elif [ "prefix=${line#'prefix='}" = "$line" ]; then
        # command prefix
        prefix=${line#"prefix="}
    elif [ "config_arguments=${line#'config_arguments='}" = "$line" ]; then
        # configuration arguments
        configargs=${line#"config_arguments="}
    elif [ "build_arguments=${line#'build_arguments='}" = "$line" ]; then
        # building arguments
        buildargs=${line#"build_arguments="}
    elif [ "install_prefix=${line#'install_prefix='}" = "$line" ]; then
        # install prefix
        install_prefix=${line#"install_prefix="}
    elif [ "$line" = "[endprogram]" ]; then
        # build logic
        cd "$progname"
        # check for new commits
        git fetch
        commitcount=$( git log ..origin/HEAD --oneline | wc -l )
        if [ $commitcount -ne 0 ] || [ "$1" = $progname ]; then
            # update
            echo "[UPDATER] Updating $progname"
            if ! [ "$1" = $progname ]; then
                git pull origin
            fi
            if [ $build_system = "zig" ]; then
                # zig build system
                if [ "$prefix" = "time" ]; then
                    time zig build install --prefix $install_prefix --summary all $configargs $buildargs
                else
                    zig build install --prefix $install_prefix --summary all $configargs $buildargs
                fi
            elif [ $build_system = "cmake" ]; then
                # cmake build system
                cmake . -B build -DCMAKE_INSTALL_PREFIX=$install_prefix $configargs
                cd build
                if [ "$prefix" = "time" ]; then
                    time cmake --build . --target install $buildargs
                else
                    cmake --build . --target install $buildargs
                fi
                cd ..
            elif [ $build_system = "automake" ]; then
                ./configure --prefix=$install_prefix $configargs
                if [ "$prefix" = "time" ]; then
                    time make
                else
                    make
                fi
                make install
                cd ..
            fi
        else
            # nothing to update
            echo "[UPDATER] $progname is already up-to-date"
        fi
        cd ..
    fi
done

echo "[UPDATER] Finished!"
