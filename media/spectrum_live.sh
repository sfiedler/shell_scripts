#!/bin/bash

# A simple spectral view video live player using FFMpeg and FFPlay
# 2024 by Samuel Fiedler

if [ -z $1 ]; then
  echo "Usage: "
  echo "  spectrum.sh INPUT_FILE"
  exit 1
fi

# Let's do everything via pipes!
ffmpeg -i $1 -filter_complex "showspectrum=size=1280x720:mode=combined:slide=scroll[spectrum]; [0:a]amix=inputs=1[audio]" -map "[spectrum]" -map "[audio]" -c:v libx264 -preset ultrafast -c:a aac -f mpegts -tune zerolatency - | ffplay -
