#!/bin/bash

# A simple spectral view video creator using FFMpeg
# 2024 by Samuel Fiedler

if [ -z $1 ] || [ -z $2 ]; then
  echo "Usage: "
  echo "  spectrum.sh INPUT_FILE OUTPUT_FILE"
  exit 1
fi

ffmpeg -i $1 -filter_complex "showspectrum=size=1280x720:mode=combined:slide=scroll[v]; [0:a]amix=inputs=1[a]" -map "[v]" -map "[a]" $2
