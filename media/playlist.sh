#!/usr/bin/env bash

# Playlist Framework
# 2023 by Samuel Fiedler

# General Playlist Setup

player="ffplay"
player_flags="-autoexit -hide_banner"
playlist_prefix="playlist:"
playlist_folder="playlists"
audio_folder="audios"

start_dir=$( dirname "$0" )
list_name=""
start_index=1
arg_state="none"

for arg in $@; do
  if [ $arg_state = "start_arg" ]; then
    start_index=$arg
    arg_state="none"
  elif [ $arg_state = "player_arg" ]; then
    player=$arg
    player_flags=""
    arg_state="none"
  elif [ $arg = "--start" ]; then
    arg_state="start_arg"
  elif [ $arg = "--player" ]; then
    arg_state="player_arg"
  else
    list_name=$arg
  fi
done

cd "$start_dir"

if ! [ -f "$playlist_folder/$list_name.txt" ]; then
  echo "Usage: "
  echo "  list.sh list_name [ --start start_index ] [ --player player_executable ]"
  exit 1
fi

readarray files < "$playlist_folder/$list_name.txt"

i=1
for file in ${files[@]}; do
  if [ $i -ne $start_index ]; then
    (( i++ ))
    continue
  fi
  playlist_name=${file#"$playlist_prefix"}
  if [[ -f "$playlist_folder/$playlist_name.txt" ]]; then
    echo "Found playlist, playing sub-list $playlist_name"
    bash $0 $playlist_name
  else
    # old_win_count=$(wmctrl -l | wc -l)
    $player $player_flags "$audio_folder/$file"
    # while [[ $(( old_win_count + 1 )) -ne $(wmctrl -l | wc -l) ]]; do
    #   sleep 0.1
    # done
    # sleep 0.1
    # wmctrl -r :ACTIVE: -t 0
    # wait $!
  fi
  if [ $? != 0 ]; then 
    break
  fi
done
