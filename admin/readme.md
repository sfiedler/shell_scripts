System Administration / Maintenance Shell Scripts
=================================================

`exec.sh`
-------------

A program updater using git.  
You'll have to place this script into a directory where you have all your git repositories you
want to update. In this directory, you'll have to create a file named "exec.ini", which contains
entries like the following:

```ini
[program]
program_name=YOUR_PROGRAM_NAME
build_system=YOUR_BUILD_SYSTEM
prefix=PREFIX_TO_RUN_COMMANDS_WITH
config_arguments=ARGUMENTS_FOR_CONFIGURATION_STEP
build_arguments=ARGUMENTS_FOR_BUILD_STEP
install_prefix=DIRECTORY_WHERE_YOU_WANT_TO_INSTALL_YOUR_PROGRAM_TO
[endprogram]
```

You can skip, if you want, the lines "prefix", "config_arguments" and "build_arguments". But
`[endprogram]` is very important, as this is the line that tells the script "please build the
program".  
You can also name the file "exec_big.ini", but then you'll have to pass the argument "--big" to
`./exec.sh`, so you can't anymore specify a specific program to build. 

The script will first cd into `$program_name` and check for newer git commits that have to be
pulled.  
If there are new commits or the first argument delivered to the script matches the program name,
the script will cd into `$program_name`. There, it'll build the program in your repo with the
build system you selected. Currently supported build systems are the cmake, automake (autoconf)
and the zig (build.zig) build system.  
`$prefix` is not prepended automatically, it is just checked whether it's "time" or not (and then
the script will run the command with "time" in front of the command or normally).
