Multimedia Shell Scripts
========================

`playlist.sh`
-------------

A simple multimedia playlist player. You will have to place the shell script in a folder that
contains a subfolder named "audios" and a subfolder named "playlists" (if you want, you can
change the folder names in line 11 and 12 of the shell script).  
The playlist folder has to contain text files (.txt) where their names are the playlist name. So
if I wanted to play a list "funny", I had to create a file "playlists/funny.txt" with all media
files listed in correct order. Also, you can play a playlist from a playlist using
"playlist:$PLAYLIST" (example: `playlist:funny`).  
Every entry is delimited by a newline (or anything other that can be processed by `readarray`).  
You can start the playlist script using `./playlist.sh PLAYLIST_NAME`. You also can `alias` the
playlist, so you don't have to enter the file path all the time. 

`spectrum.sh`
-------------

A (very simple) audio / video to spectral view video converter.  
Usage: `spectrum.sh INPUT OUTPUT`

`spectrum_live.sh`
------------------

Basically the same as `spectrum.sh`, except that it pipes the output directly to ffplay and that
it has a lower latency.  
Usage: `spectrum_live.sh INPUT`
