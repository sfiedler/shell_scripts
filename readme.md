Shell Scripts
=============

Some (maybe) useful bash shell scripts. This work is licensed under the MIT License. 

This repository will have subfolders with shell scripts in the subfolders. Also, you will find
some instructions on how to use the shell scripts in the "readme.md" file of that subfolder. 
